//comentario de 1 linha
/*
bloco de comentario
*/

let numero = 0;

while(numero<=10){
    
    if(numero%2==0){
        console.log(`valor nr: ${(numero)} é par`);
    }else{
        console.log(`valor nr: ${(numero)}`);
    }
    numero++;
}

console.log ("Repetição DO/While");
let numero1 = 0;
do{
    console.log(`valor nr: ${(numero1)}`);
    numero1++;
}while(numero1<=10);